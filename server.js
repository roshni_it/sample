const express = require('express');
const app = express();
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
const port = process.env.PORT || 3000;

// Database Connectivity
const config = require('./config/database', {server: { poolSize: 5 }});
mongoose.connect(config.database);
    //Check Connection
    mongoose.connection.on('connected', function(){
        console.log('App successfully connected with DB: '+ config.database);
    });
    mongoose.connection.on('error', function(err){
        console.log('Database Connection Error: '+ err);
    });

// Add Static Folder To add Frontend Design
app.use(express.static(path.join(__dirname, 'client')));


// Add cors Moddleware
app.use(cors());

// Add body-parser middleware 
app.use(bodyParser.json());

// Create Routers for API
const users = require('./routers/users');
app.use('/userapi', users);

// Front page Load on URL: http://localhost:3000
/*app.use('/', function(req, res){
    res.send('App Running Successfully');
}); */

app.get('*', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/client/index.html')); // Set index.html as layout
});

//Add Passport For Authentication to protect certain routes like user profile
const passport = require('passport');
app.use(passport.initialize());
app.use(passport.session());

// add Passport.js File
require('./config/passport')(passport);

// Listen Port
app.listen(port);
console.log('Blog App running on port: ' + port);